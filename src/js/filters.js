/**
* Created by Jessica Stamos (https://jessicastamos.com)
* Refer to the repo for updates/additional information
* https://gitlab.com/jnstamos/vanilla-js-multiple-filters
* Contact: jessica@jessicastamos.com
*/

const jsFilters = (() => {
  const makeArray = (element) => { return Array.prototype.slice.call(element); };

  // The masonry works in IE9
  // Begin masonry code for filters (could be reused elsewhere)
  const filterMasonry = (element) => {
    // Return to default for when window resizes to calculate correct width
    element.style.maxWidth = '';
    const containerWidth = element.offsetWidth;
    element.style.maxWidth = `${containerWidth}px`;
    const items = makeArray(element.querySelectorAll('.filters__container'));
    const columns = parseInt(element.getAttribute('data-columns'));
    const allItemHeights = [];
    let heightsToRows = [];

    // Return items position for when window resizes to calculate correct positions
    items.forEach((item) => item.style.position = 'static');

    items.forEach((item, index, arr) => {
      const itemWidth = item.offsetWidth;
      let itemHeight = item.offsetHeight;
      if(item.style.display === 'none') {
        item.style.display = 'inline-block';
        itemHeight = item.offsetHeight;
        item.style.display = 'none';
      }
      allItemHeights.push(itemHeight);

      if((itemWidth * index) < containerWidth) {
        item.style.left = `${item.offsetLeft}px`;
        item.style.top = `${item.offsetTop}px`;
      } else {
        item.style.left = `${item.offsetLeft}px`;
        item.style.top = `${parseInt(arr[(index-columns)].style.top)+arr[(index-columns)].offsetHeight}px`;
      }
    });

    items.forEach((item) => item.style.position = 'absolute');

    // Below functions will get each height by column and add them together to get a max height
    // that will be applied to the overall container to make sure it's sized correctly for the items

    // Split heights into rows by splicing at column end
    for(let i = 0; i < allItemHeights.length; i += columns) {
      heightsToRows.push(allItemHeights.slice(i, i + columns));
    }

    // Get the number of rows
    const numberRows = Math.round(allItemHeights.length/columns);
    // Empty array with row size slots created to add heights of each column by row
    const rowHeightsToColumns = new Array((Math.round(numberRows)));
    // Create a dummy array from columns to map into keys for correct for each iteration
    const columnArrayCount = "".split.call(Array(columns), ",");
    // Array to hold keys for forEach loop
    const columnKeys = [];
    // Map keys to create an array of 'range'
    columnArrayCount.forEach((value, index) => columnKeys.push(index));

    // For each height in each column, add height from row to corresponding column in array
    heightsToRows.forEach((result) => {
      columnKeys.forEach((num) => {
        rowHeightsToColumns[num] +=  ', ' + result[num];
      });
    });

    const maxColumnHeights = [];
    rowHeightsToColumns.forEach((columnArr) => {
      // Since values are a string, split at ', ', put them as numbers and remove any 'NaN'
      columnArr = columnArr.split(', ').map(Number).filter(Boolean);
      let sum = 0;
      // for each column's array, add the values together and put into an array
      columnArr.forEach((value) => {
        sum += value;
      });
      maxColumnHeights.push(sum);
    });

    // Get the max height of a column
    const containerHeight = Math.max.apply(null, maxColumnHeights);
    // Get the height of the filter list
    const filterListHeight = element.querySelector('ul').offsetHeight;
    // Add the max column height and filter list height and apply it as the height of the container
    element.style.height = `${containerHeight + filterListHeight}px`;
  };

  // Apply masonry to filters
  const filtersMasonry = makeArray(document.querySelectorAll('.filters'));

  // Prevents first load up issue of content being positioned over each other
  window.onload = () => { filtersMasonry.forEach((filterContainer) => filterMasonry(filterContainer)); };
  // To prevent mobile issues, only fire resize function if windowWidth is different
  const windowWidth = window.innerWidth;
  window.addEventListener('resize', () => {
    if(windowWidth != window.innerWidth) {
      filtersMasonry.forEach((filterContainer) => filterMasonry(filterContainer));
    }
  });

  // Begin filter functions
  const masonryFiltered = (array, parent) => {
    array.forEach((item, index, arr) => {
      const containerWidth = parent.offsetWidth;
      const itemWidth = item.offsetWidth;
      const columns = parseInt(parent.getAttribute('data-columns'));

      if((itemWidth * index) < containerWidth) {
        item.style.left = `${item.offsetLeft}px`;
        item.style.top = `${item.offsetTop}px`;
      } else {
        item.style.left = `${item.offsetLeft}px`;
        item.style.top = `${parseInt(arr[(index-columns)].style.top)+arr[(index-columns)].offsetHeight}px`;
      }
    });

    array.forEach((item) => item.style.position = 'absolute');
  };

  const removeActiveFilter = (active, clicked) => {
    if(active && clicked) {
      if(active.getAttribute('data-filter') != clicked.getAttribute('data-filter')) {
        active.className = active.className.replace('filters--active', '');
      }
    } else if(active) {
      active.className = active.className.replace('filters--active', '');
    }
  };

  const displayHiddenItems = (array) => {
    array.forEach((item) => {
      item.style.position = 'static';
      item.style.display = 'inline-block';
      item.removeAttribute('data-filtered');
    });
  };

  const filterItems = (clicked) => {
    const filter = clicked.target;
    const category = filter.getAttribute('data-filter');
    const filterContainer = filter.parentNode;
    const parent = filterContainer.parentNode;
    const allItems = makeArray(parent.querySelectorAll(`[data-category]`));
    const filtered = makeArray(parent.querySelectorAll(`[data-category*="${category}"]`));
    let activeFilter = filterContainer.querySelector('.filters--active');
    const clearFilter = parent.querySelector('[data-filter="clear"]');

    console.log(filtered);

    allItems.forEach((item) => {
      item.style.transition = 'left .3s ease-in-out, top .3s ease-in-out';
    });

    if(filter.getAttribute('data-filter') != 'clear') {
      filter.className = 'filters--active';
      removeActiveFilter(activeFilter, filter);
      // Remove items for re-toggling...
      displayHiddenItems(allItems);

      // Begin filtering...
      filtered.forEach((item) => {
        item.setAttribute('data-filtered', '');
      });

      allItems.forEach((item) => {
        // Set position to default to get correct positioning
        item.style.position = 'static';

        // Hide filtered items
        if(!item.hasAttribute('data-filtered')) {
          item.style.display = 'none';
        }
      });

      masonryFiltered(filtered, parent);

      window.addEventListener('resize', () => {
        if(windowWidth != window.innerWidth) {
          // Stop transitioning of position on resize
          allItems.forEach((item) => {
            item.style.transition = 'none';
          });

          displayHiddenItems(allItems);

          // Begin filtering...
          filtered.forEach((item) => {
            item.setAttribute('data-filtered', '');
          });

          allItems.forEach((item) => {
            // Set position to default to get correct positioning
            item.style.position = 'static';

            // Hide filtered items
            if(!item.hasAttribute('data-filtered')) {
              item.style.display = 'none';
            }
          });

          masonryFiltered(filtered, parent);
        }
      });
    }

    clearFilter.style.display = 'inline-block';
    clearFilter.addEventListener('click', () => {
      clearFilter.style.display = 'none';
      activeFilter = filterContainer.querySelector('.filters--active');
      removeActiveFilter(activeFilter);
      displayHiddenItems(allItems);
      masonryFiltered(allItems, parent);
      window.addEventListener('resize', () => {
        if(windowWidth != window.innerWidth) {
          displayHiddenItems(allItems);
          masonryFiltered(allItems, parent);
        }
      });
      clearFilter.className = 'filters--hidden';
    });
  };

  const masonryFilter = makeArray(document.querySelectorAll('.filters ul li'));
  masonryFilter.forEach((filter) => filter.addEventListener('click', filterItems));
})();